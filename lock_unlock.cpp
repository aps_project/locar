#include <SPI.h>
#include <MFRC522.h>
#include <Servo.h>
#include <Arduino.h>

#include "lock_unlock.h"

#define locked A1    
    
#define OTVORENE A2  
#define ZATVORENE A3

#define RST_PIN 8
#define SS_PIN 9

Servo myservo; 
MFRC522 citacka(SS_PIN, RST_PIN);


void setup_lock_unlock() {
  pinMode(locked, INPUT);
  pinMode(OTVORENE, OUTPUT);
  pinMode(ZATVORENE, OUTPUT);
  digitalWrite(ZATVORENE,HIGH);

  Serial.begin(9600);
  while (!Serial);
  SPI.begin();
  citacka.PCD_Init();

  myservo.attach(A0);
  myservo.write(0);

  delay(500);
  myservo.detach();
  
}

void lock_unlock_main(char user[10], char users[][10], char uid[][10]){
  // nacita id karty
  char tag_id[10];

  // porovnavanie a kontrola id prilozenej karty
  if (getID(tag_id) && checkID(tag_id, uid)) {
    // ak je stlacene tlacidlo, odomkne
    if(digitalRead(locked) == HIGH){
        // zmena farby ledky
        digitalWrite(OTVORENE,HIGH);
        digitalWrite(ZATVORENE,LOW);
        // vycistenie aktualneho pouzivatela
        memset(user, '\0', 10);
        // nacita aktualneho pouzivatela
        int i = 0;
        while (i < 10 && uid[i][0] != '\0'){
          if (strcmp(tag_id, uid[i]) == 0){
            strcpy(user, users[i]);
            break;   
          }
          i++;
        }
        // otocenie motorom
        myservo.attach(A0);
        myservo.write(180); //otvori zamok
        delay(500);
        myservo.detach();
    } else {
        // zmena farby ledky
        digitalWrite(OTVORENE,LOW);
        digitalWrite(ZATVORENE,HIGH);
        // vycistenie aktualneho pouzivatela
        memset(user, '\0', 10);
        strcpy(user, "NO_USER");
        // otocenie motorom
        myservo.attach(A0);
        myservo.write(0);  //zatvori zamok
        delay(500);
        myservo.detach();
    }
  }
}

void only_lock_unlock(){
  // kontrola stlacenia tlacidla indikujuce zamknutie/odomknutie
  if(digitalRead(locked) == HIGH){
    // zmena farby ledky
    digitalWrite(OTVORENE,HIGH);
    digitalWrite(ZATVORENE,LOW);
    // otocenie motorom
    myservo.attach(A0);
    myservo.write(180); //otvori zamok
    delay(500);
    myservo.detach();
  } else {
    digitalWrite(OTVORENE,LOW);
    digitalWrite(ZATVORENE,HIGH);
    // otocenie motorom
    myservo.attach(A0);
    myservo.write(0);  //zatvori zamok
    delay(500);
    myservo.detach();
  }
}

bool getID(char id_buf[]){
  String tagID;
  // kontrola, ci sa karta nachadza pri citacke
  if ( ! citacka.PICC_IsNewCardPresent()){
    Serial.println("not new card");
    return false;
  }

  // kontrola precitania serioveho cisla karty
  if ( ! citacka.PICC_ReadCardSerial()){
    Serial.println("can not read id card");
    return false;
  }

  // nacitanie uid do premennej tagID
  for ( uint8_t i = 0; i < 4; i++) {
    tagID.concat(String(citacka.uid.uidByte[i], HEX));
  }
  tagID.toUpperCase();
  
  Serial.println(tagID);
  
  // vlozenie uid karty do id_buff
  tagID.toCharArray(id_buf, 10);
  citacka.PICC_HaltA();
  return true;
}

bool checkID(char tag_id[10], char uid[][10]){
  byte i = 0;
  // validacia uid prilozenej karty
  while(i < 10 && uid[i][0] != '\0'){
    Serial.print("original: ");
    Serial.println(uid[i]);
    Serial.print("uid: ");
    Serial.println(tag_id);
        
    if (strcmp(tag_id, uid[i]) == 0){  
      Serial.println("zhoda uid");
      return true;
    }
    i++;
  }
  Serial.println("nezhoda uid");
  return false;  
}
