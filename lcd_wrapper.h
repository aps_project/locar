#ifndef _LCD_H
#define _LCD_H

/**
 * Inicializacia lcd displeju
 */
void lcd_init();


/**
 * Vycisti lcd  displeju
 * 
 * Vycisti obsah lcd displju. Po zavolani je displej prazdny.
 */
void lcd_clear();


/**
 * Nastavi kurzor na danu poziciu.
 * 
 * Nastavi kurzor na zaklade danej x a y suradnice.
 * @param y riadok 
 * @param x stlpec
 */
void lcd_set_cursor(byte y, byte x);


/**
 * Vypise text na displej.
 * 
 * Vypise text na lcd displej na pozicii na ktorej sa prave nachadza kurzor.
 * @param text text na vypis
 */
void lcd_print(char* text);


/**
 * Nastavi kurzor na poziciu a vypise text.
 * 
 * Nastavi kurzor na poziciu danou x, y suradnicou a vypise text.
 * @param y riadok
 * @param x stlpec
 * @param text text na vypis
 */
void lcd_print_at(byte y, byte x, char* text);

#endif
