#include <Arduino.h>
#include <Keypad.h>
#include <string.h>

#include "keypad.h"
#include "lcd_wrapper.h"
#include "lock_unlock.h"

// inicializacia znakov na klavesnici
const char keys[4][4] = {

//  {'1','2','3','E'},
//
//  {'4','5','6','U'},
//
//  {'7','8','9','D'},
//
//  {'*','0','#','B'}

  {'L','7','4','*'},

  {'0','8','5','2'},

  {'#','9','6','3'},

  {'B','D','U','E'}

};

// inicializacia keypadu
byte rowPins[4] = {3, 2, A4, A5}; //piny pre riadky klavesnice
byte colPins[4] = {7, 6, 5, 4};   //piny pre stlpce klavesnice
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, 4, 4);

char* get_text(byte lcd_x_pos, byte lcd_y_pos, char* header) {
	// inicializacia premennych
	char* text = (char*) calloc(10, sizeof(char));
	byte pos = 0;
	const unsigned short TIMEOUT = 1000;
	char letter;
	char old_key;
	bool other_key = false;
	byte letter_pos;
	char key = NO_KEY;

	while(key != 'E'){
    // lcd vypis
		lcd_clear();
		lcd_print_at(0, 0, header);
		lcd_print_at(lcd_y_pos, lcd_x_pos, text);

    // caka na stlacenie klavesy 10 sekund ak nebola stlacena ina klavesa
		if(!other_key){
			key = wait_for_key(10000);
      if (key == NO_KEY) {
        // ak sa nestlacila klavesa, vracia *HOME (navrat do hlavneho os loopu)
        strcpy(text, "*HOME");
        return text;
      }
		}

    // resetovanie premennych
		other_key = false;
		letter_pos = 0;
		old_key = key;

    // je v cykle, ak stale stlacame tu istu klavesu
		while(key == old_key && key != 'E'){

      // ziskame pismeno podla stlacenej klavesnice
			letter = get_letter(key, letter_pos);
						
			// --------------------------------
			// KONTROLA PISMENA
			// --------------------------------

			// vymazania pismena
			if (letter == '*'){
				break;
			}

      // navrat do menu
			if (letter == 'b') {
        free(text);
				return NULL;
			}

      // bola stlacena klavesa urcena pre pismenka
			if (letter != 'x'){
        // aktualizuje text na pozicii a vypise ho na lcd
				update_text(text, letter, pos);
				lcd_print_at(lcd_y_pos, lcd_x_pos, text);	
				letter_pos++;
			}

      // cakanie na stlacenie klavesy 1 sekundu
			key = wait_for_key(TIMEOUT);
		}

		if(key != NO_KEY && key != 'E'){
      // riesenie vymazania
			if (key == '*'){
				if (old_key == '*'){
					if (pos > 0){
						if (text[8] == '\0'){ 
						  pos--;
						}
            update_text(text, '\0', pos);
					}
				}
				else {
					update_text(text, '\0', pos);
				}
				continue;
			}
      // navrat do menuu
			else if (key == 'B'){
        free(text);
				return NULL;
			}
      // stlacena klavesa bez vyznamu -> pokracuje v cykle
			else if (letter == 'x'){
				continue;
			}
      // stlacena validna klavesa pre pismenka
			else {
				other_key = true;
			}

		}
		// zvysenie poziciie v texte ak nebolo dosiahnute maximum
		if (pos < 8){
			pos++;
		}
		
	}

  // kontrola dlzky textu
  if (strlen(text) < 4) {
    // lcd vypis
    lcd_clear();
    lcd_print_at(0, 0, (char*) "Name is not");
    lcd_print_at(1, 2, (char*) "long enough");
    delay(2000);
    return NULL;
  }
  
	return text;
}

void update_text(char* text, const char letter,byte pos){
  	text[pos] = letter;
}

char wait_for_key(const unsigned short timeout){
	// inicializacia premennych
	unsigned int time_start;
	unsigned int time_end;
	char key;
	time_start = millis();
	time_end = time_start;

  // caka na stlacenie klavesy timeout milisekund
	while(time_end - time_start < timeout) {
		key = keypad.getKey();
		if (key != NO_KEY){
			return key;
		}
		time_end = millis();
	}
	return NO_KEY;
}

char no_key(){
	return NO_KEY;
}


char get_letter(const char key, const byte pos) {
  // vrati pismeno na zaklade stlacenej klavesnice a premennej pos (pozicia)
	char letters[5];
	switch(key){
		case '2':
			strcpy(letters, "ABC");
			return letters[pos % 3];
		case '3':
			strcpy(letters, "DEF");
			return letters[pos % 3];
		case '4':
			strcpy(letters, "GHI");
			return letters[pos % 3];
		case '5':
			strcpy(letters, "JKL");
			return letters[pos % 3];
		case '6':
			strcpy(letters, "MNO");
			return letters[pos % 3];
		case '7':
			strcpy(letters, "PQRS");
			return letters[pos % 4];
		case '8':
			strcpy(letters, "TUV");
			return letters[pos % 3];
		case '9':
			strcpy(letters, "WXYZ");
			return letters[pos % 4];
		case '*':
			return '*';
		case 'B':
			return 'b';
		default:
			return 'x';

	}
}
