#ifndef _OS_H
#define _OS_H

/**
 * Spustenie "operacneho systemu" jeden krat
 * 
 * Spusti jeden krat "operacny system". Caka na stlacenie klavesy isty cas.
 * Ak sa stlacila klavesa(ak stlacena * - odomkne/zamkne dvere auta), dostaneme
 * sa do menu, kde su na vyber tri moznosti pridat pouzivatela,
 * vymazat uzivatela a zobrazit uzivatelov.
 * 
 * Pridanie uzivatela:
 *    Budeme vyzvany na zadanie mena (aspon 4 znaky), nasledne musime oskenovat
 *    kartu uzivatela.
 * Vymazanie pouzivatela:
 *    Zobrazi zoznam so vsetkymi pouzivatelmi. Oznaceneho pouzvatela vymaze
 *    z SD karty.
 * Zobrazenie uzivatelov:
 *    Zobrazi zoznam uzivatelov.
 *    
 * @param user aktualny pouzivatel
 * @param users vsetci ulozeni pouzivatelia
 * @param uid vsetky ulozene UID kariet uzivatelov
 */
void os_main(char* user, char users[][10], char uid[][10]);

/**
 * os_main v cykle
 * 
 * Najprv sa vykonaju nastavenia: nacitanie vsetkych pouzivatelov a ich UID kariet
 * do premennych. Potom sa spusti nekonecny cyklus v ktorom je funkcia os_main
 * a lock_unlock_main z modulu lock_unlock.
 */
void os_run();

/**
 * Riesenie menu vyber
 * 
 * Riesi, zabezpecuje spracovanie polozky vyberu z menu a navrat do hlavneho
 * cyklu (os_run()).
 * 
 * @param users vsetci ulozeni pouzivatelia
 * @param uid vsetky ulozene UID kariet uzivatelov
 */
void handle_menu(char users[][10], char uid[][10]);

/**
 * Prida uzivatela do systemu
 * 
 * Riesi polozku menu add user. Pouzivatel bude vyzvany na zadanie mena
 * (aspon 4 znaky), nasledne musi oskenovat kartu.
 * Ak nastane neaktivita pri zadavani mena alebo skenovani karty, po
 * 10 sekundach sa spusti hlavny cyklus (while v os_run).
 * 
 * @param users vsetci ulozeni pouzivatelia
 * @param uid vsetky ulozene UID kariet uzivatelov
 * @return -1 pri neaktivite alebo 1 pri stlaceni klavesy B (back)
 * 
 */
int add_user(char users[][10], char uid[][10]);

/**
 * Zobrazi vsetkych pouzivatelov
 * 
 * Zobrazi zoznam vsetkych pouizvatelov (po jednom)na LCD displeji.
 * Nekativita 10 sekund v zozname znamena navrat do hlavneho cyklu v os_run.
 * 
 * @param users vsetci ulozeni pouzivatelia
 * @return index potvrdeneho usera pri stlaceni E alebo -2 pri stlaceny
 *         klavesy navrat (B - back) alebo -1 pri neaktivite.
 */
int show_users(char* users);

/**
 * Odstrani uzivatela zo systemu
 * 
 * Odstrani uzivatela zo systemu na zaklade mena. Zobrazi sa zoznam s uzivatelmi
 * a potvrdenim (stlacenim klavesy E) sa potvrdeny uzivatel vymaze zo systemu.
 * Nekativita 10 sekund v zozname znamena navrat do hlavneho cyklu v os_run.
 * 
 * @param users vsetci ulozeni pouzivatelia
 * @param uid vsetky ulozene UID kariet uzivatelov
 */
int remove_user(char users[][10], char uid[][10]);

/**
 * Riesi navigaciu v menu a aka polozka bola vybrata z menu.
 * @return index vybranej polozky z menu
 */
int menu_selection();

/**
 * Vypise jednu polozku z menu
 * 
 * @param menu_text menu polozka
 */
void print_menu(char* menu_text);

/**
 * Vypise jedneho pouzivatela
 * 
 * @param user uzivatel na vypisanie na LCD displej
 */
void print_user(char* user);

#endif
