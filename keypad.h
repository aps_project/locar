#ifndef _KEYPAD_H
#define _KEYPAD_H

/*
**	Funckia ktora riesi kompletnu funkcionalitu pisania textu.
** 	(tak ako stare tlacitkove mobily avsak iba pismena bez medzery)
**
**  Parametre:
**		@param lcd_x_pos - x pozicia vypisu textu na display
**		@param lcd_y_pos - y pozicia vypisu textu na display
** 		@param header - text ktory sa vypise na pozicii [0,0]
**
**  - Pri stlaceni klavesy ktora nema momentalne pridelenu ziadnu funkciu,
**		ignoruje stlacenie.
**	- Pri stlaceni vymazavacej klavesy(*), vymaze posledne pismeno.
** 	- Pri stlaceni Enter klavesy, vracia napisany text.
** 	- Pri stlaceni Back klavesy, vracia NULL
*/
char* get_text(byte lcd_x_pos, byte lcd_y_pos, char* header);

/*
**	Funkcia updatuje text pri stlacani klavesnice.
** 	(Vklada pismeno letter na poziciu pos do textu text)
*/
void update_text(char* text, const char letter, byte pos);

/*
**	Funkcia na zaklade dvoch parametrov, stlacena klavesa a pozicia
**	pismena na klavesnici, vracia pismeno na klavesnici na danej pozicii.
*/
char get_letter(const char key, const byte pos);

/*
** 	Funkcia caka na stlacenie klavesy timeout milisecond.
**
** 	Vracia stlacenu klavesu typu char alebo NO_KEY(ak ubehne timeout).
*/
char wait_for_key(const unsigned short timeout);

/*
** 	Funkcia vracia konstantu NO_KEY.
** 	Vytvorena preto, aby tuto konstantu mohli pouzivat aj ine moduly.
*/
char no_key();

#endif
