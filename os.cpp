#include <Arduino.h>
#include <SD.h>

#include "keypad.h"
#include "os.h"
#include "lcd_wrapper.h"
#include "lock_unlock.h"
#include "locar_database.h"

int menu_selection(){
	// inicializacia premennych
	byte max_menu = 3;
	char menu_list[max_menu][12] = {"Add user", "Remove user", "Show users"};
	byte menu = 0;
  char key = no_key();
  
  // vypise prvu polozku
	print_menu(menu_list[menu]);  

  // cyklus sa opakuje dokym nie je stlacena klavesa E -> potvrdenie a B -> navrat
	while(key != 'E' && key != 'B') {
    // ak je klavesa U, posunieme sa v menu smerom hore ak nie sme na konci
		if (key == 'U' && menu < max_menu - 1){
			menu++;
		} // ak je klavesa D, posunieme sa v menu smerom dole ak nie sme na zaciatku
		else if (key == 'D' && menu > 0) {
			menu--;
		}	

    // zobrazi polozku na ktorej sa nachadzame
		print_menu(menu_list[menu]);

    // caka 10 sekund, ak sa nestlaci klavesa, vracia -1 -> presun do hlavneho os loopu
		key = wait_for_key(10000);
		if (key == no_key()){
			return -1;
		}
	}

	if(key == 'E'){
    // ak bolo stlacene E, vraca index polozky
		return menu;
	} else {
    // ak B, vracia -1 -> navrat do main os loopu
		return -1;
	}

}

int add_user(char users[][10], char uid[][10]){
  // vypis na lcd
	lcd_clear();
	lcd_print_at(0, 0, (char*) "Set user name:");

  // nacitanie mena noveho pouzivatela cez keypad
	char* new_user = get_text(2,1, (char*) "Set user name:");

  // ak je new_user null, vracia 1 (navrat do menu)
	if (new_user == NULL) {
		return 1;
	// ak je new_user *HOME, vracia -1 (naspat do hlavneho os loopu)
	} else if (strcmp(new_user, "*HOME") == 0){
    free(new_user);
    return -1;
	}

  // inicializacia premennych po uspesnom nacitani mena noveho pouzivatela
	char card_uid[10];
  short time_start = millis();
  short time_end = time_start;
  bool loaded = false;
  // vypis na lcd
  lcd_clear();
  lcd_print_at(0, 0, (char*) "Scan card for");
  lcd_print_at(1, 1, (char*) "user");
  lcd_print_at(1, 6, new_user);

  // zaka na nacitanie karty 10 sekund
	while(!(loaded = getID(card_uid)) && (time_end - time_start < 10000)){
    time_end = millis();
	}

  // ak sa karta nenacitala, vracia -1 (navrat do hlavneho os loopu)
  if (!loaded){
    // lcd vypis
    lcd_clear();
    lcd_print_at(0, 0, (char*) "User");
    lcd_print_at(0, 5, new_user);
    lcd_print_at(1, 0, (char*) "not added");
    free(new_user);
    delay(2000);
    return -1;
  }

  // zapise uzivatela do suboru na sd karte
	addUser(new_user, card_uid);
  // nacita pouzivateov a id kariet
  loadUsers(users, uid);

  // lcd vypis
	lcd_clear();
	lcd_print_at(0, 0, (char*) "User");
	lcd_print_at(0, 5, new_user);
	lcd_print_at(1, 6, (char*) "added");
	delay(2000);
	free(new_user);
	return 0;
}

int show_users(char users[][10]){
  // inicializacia premennych
  byte users_len = 0;
  while(users[users_len][0] != '\0'){
    users_len++;
  }
	byte user_idx = 0;
  char key = no_key();
  
  // kontrola poctu pouzivatelov
  // ak premenna users neobsahuje mena pouzivatelov,
  // vypise na lcd displej spravu a vrati -2 (navrat do menu)
  if (users_len == 0) {
    lcd_clear();
    lcd_print_at(0, 2, (char*) "NO USERS");
    delay(2500);
    return -2; 
  }
  
  // vypise prveho usera
	print_user(users[user_idx]);

  // cyklus sa opakuje dokym nie je stlacena klavesa E -> potvrdenie a B -> navrat
	while(key != 'B' && key != 'E'){
    // ak je klavesa U, posunieme sa v menu smerom hore ak nie sme na konci
		if (key == 'U' && user_idx < users_len - 1){
			user_idx++;
		}
    // ak je klavesa D, posunieme sa v menu smerom dole ak nie sme na zaciatku
		else if (key == 'D' && user_idx > 0) {
			user_idx--;
		}	

    // vypise pouzivatela podla user_idx
		print_user(users[user_idx]);

    // caka na stlacenie klavesy 10 sekund
		key = wait_for_key(10000);

    // ak nestlacena klavesa -> vracia -1 (navrat do hlavneho os loopu)
		if (key == no_key()){
			return -1;
		}
	}

  // ak stlacene E -> vracia index pozivatela
  if(key == 'E'){
    return user_idx;
  }
  // inak vracia -2 (lebo 1 je index pouzivatela)
	return -2;
}

int remove_user(char users[][10], char uid[][10]){
  // ziskanie index pouzivatela na vymazanie pomocou show_users
  int user_idx = show_users(users);

  // overenie vyberu pouzivatela
  if (user_idx == -1) {
    return -1;
  } else if (user_idx == -2){
    return 1;
  }
  
  // ziskanie mena pouzivatela
  char user[10];
  strcpy(user, users[user_idx]);

  // odstranenie uzivatela z sd karty
  deleteUser(user, users, uid);

  // lcd vypis
  lcd_clear();
  lcd_print_at(0, 0, (char*) "User:");
  lcd_print_at(0, 6, user);
  lcd_print_at(1, 2, (char*) "Deleted");
  delay(2000);
  return 1;
}

void print_menu(char* menu_text){
	lcd_clear();
	lcd_print_at(0, 2, (char*) "MENU:");
	lcd_print_at(1, 4, menu_text);
}

void print_user(char* user){
	lcd_clear();
	lcd_print_at(0, 0, (char*) "USER:");
	lcd_print_at(0, 6, user);
}


void os_run(){
  Serial.println("Os is running...");
  // inicializacia premennych
  char users[10][10];
  char uid[10][10];
  String buf[10];
  char user[10] = "NO_USER";

  // nacitanie pouzivatlov a id z sd karty
  loadUsers(users, uid);

  // spustenie hlavneho cyklu
  while(1){
    // kontrola stlacenia klavesy
    os_main(user, users, uid);
    // kontrola citacky
    lock_unlock_main(user, users, uid);
  }
}

void os_main(char* user, char users[][10], char uid[][10]){
  //Serial.println(user);
  // vypise uzivatela ktory odomkol auto citackou alebo NO_USER
  print_user(user);

  // caka 1 sekundu na stlacenie klavesy
	char key = wait_for_key(1000);
	if (key != no_key()) {
		if (key == 'L') {
      // ak bola stlacena L, odomkne/zamkne dvere
			only_lock_unlock();
		} else {
      // ak bola stlacena ina ako L, spusti sa menu
			handle_menu(users, uid);	
		}
	}
		
}

void handle_menu(char users[][10], char uid[][10]){
	short menu;
	short menu_return = 1;

  // pokial bude navrat do menu rovny 1, ostaneme v menu
	while (menu_return == 1){
    // ak bude menu selection rovne -1, znamena to neaktivitu -> navrat do hlavneho os loopu
		if ((menu = menu_selection()) != (-1)){
			switch (menu){
				case 0:
					menu_return = add_user(users, uid);
					break;
				case 1:
					menu_return = remove_user(users, uid);
					break;
				case 2:
					menu_return = show_users(users);
          if (menu_return != -1) menu_return = 1;
					break;
			}
		} else {
			break;
		}
	}
}
