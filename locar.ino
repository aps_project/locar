#include "lock_unlock.h"
#include "os.h"
#include "lcd_wrapper.h"
#include "locar_database.h"

void setup() {
  Serial.begin(9600);
  Serial.println("---SETUP---");
  setup_lock_unlock();
  sdSetup();
  lcd_init();
  Serial.println("---END-SETUP---");
}

void loop() {
  Serial.println("Main loop is running..");
  os_run();
}
