#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include "locar_database.h"
#include <errno.h>

//setup sd karty
void sdSetup() {
  pinMode(SS, OUTPUT);
  if (!SD.begin(10)) {
    Serial.println("SD Card initialization failed!");
    return;
  }
  else {
    Serial.println("SD Card OK.");
  }
}

//nacita vsetkych pouzivatelov a ich uid do globalnych premennych
void loadUsers(char driver[][10], char uid[][10]) {
  //vycistim globalne premenne s udajmi
  File myFile;
  memset(driver, '\0', sizeof(driver[0][0]) * 100);
  memset(uid, '\0', sizeof(uid[0][0]) * 100);
  myFile = SD.open("test.txt", FILE_READ);
  if (myFile) {
    byte i = 0;
    //nacitavam pokym nieje EOF
    while (myFile.available() && i < 10) {
      //nacitam riadok do stringu
      String list = myFile.readStringUntil('\r');
      char buf[20];
      //zmenim string na *char (aby som mohol pouzit funkciu sscanf)
      list.toCharArray(buf, 20);
      sscanf(buf, "%s %s\n", driver[i], uid[i]);
      i++;
    }
    myFile.close();
  }
  else {
    
    // pri neuspesnom otvarani suboru:
    Serial.println("error opening test.txt in loadusers");
    Serial.println(myFile);
  }
}

//prida udaje noveho pouzivatela na koniec textfile
void addUser(String driver, String uid) {
  File myFile;
  myFile = SD.open("test.txt", FILE_WRITE);
  byte i = 0;
  //pocitam riadky
  while (myFile.available() && i < 10) {
    myFile.readStringUntil('\r');
    i++;
  }
  //ak je 10 riadkov, je aj 10 pouzivatelov a to je maximum, cize nepridam usera
  if (i == 10) {
        myFile.close();
    return;
  }
  else if (myFile) {
    myFile.print(driver);
    myFile.print(" ");
    myFile.println(uid);
    myFile.close();
  }
  else {
    // pri neuspesnom otvarani suboru:
    Serial.println("error opening test.txt in addUser");
  }
}

//vymaze pouzivatela podla parametra driver/uid
void deleteUser(char toDelete[], char driver[][10], char uid[][10]) {
  File myFile;
  //v tomto cykle si ulozim do i index riadku na vymazanie
  byte i = 0;
  while ((strcmp(toDelete, driver[i]) != 0) && (strcmp(toDelete, uid[i]) != 0 )) {
    i++;
    if (i == 10)
      break;
  }
  //ak je i==10 znamena to ze sa string na vymazanie nenasiel. prerusime funkciu
  if (i == 10) {
    return;
  }
  //skratim velkost suboru na 0 bytov(vymazem obsah)
  myFile = SD.open("test.txt", FILE_WRITE | O_TRUNC | O_APPEND);
  if (myFile) {
    //do prazdneho textfilu vlozim vsetky udaje, okrem udajov s indexom i
    byte j = 0;
    while ((driver[j][0] != '\0') && (uid[j][0] != '\0')) {
      if (j != i) {
        myFile.print(driver[j]);
        myFile.print(" ");
        myFile.println(uid[j]);
      }
      j++;
    }
    myFile.close();
    //nacitam nove udaje do globalnych premennych
    loadUsers(driver, uid);
  }
  else {
    // pri neuspesnom otvarani suboru:
    Serial.println("error opening test.txt in deleteuser");
  }
}
