#ifndef _LOCK_UNLOCK_H
#define _LOCK_UNLOCK_H

/**
 * Setup funkcia
 * 
 * Sluzi na inicializaciu zakladnych parametrov, serva, rfid citacky, pinov
 */
void setup_lock_unlock();

/**
 * Hlavna funkcia pre odmknutie/zamknutie
 * 
 * Nacita ID katy, skontroluje validitu, ak je validne,
 * odomkne/zamkne dvere a nacita aktualneho pouzivatela
 * 
 * @param user buffer pre aktualneho pouzivatela - ten, kto odomkol dvere
 * @param users zoznam mien pouzivatelov
 * @param uid zoznam uid kariet
 */
void lock_unlock_main(char user[], char users[][10], char uid[][10]);

/**
 * Zamkne/odomkne dvere auta
 */
void only_lock_unlock();

/**
 * Nacitanie ID karty do buffra
 * 
 * Skontroluje prilozenie karty, ak sa karta nenachadza v blizkosti citacky,
 * vrati false. Nacita ID karty (ak sa nepodari vrati false). Id vlozi do id_buf
 * parametra funkcie a vrati true.
 * 
 * @param id_buf buffer pre nacitane id karty
 */
bool getID(char id_buf[10]);

/**
 * Validacia ID
 * 
 * Porovna ID so vsetkymi ID v parametri uid.
 * Vrati true ak sa nasla zhoda, inak false.
 * 
 * @param tag_id ID urcene na kontrolu
 * @param uid zoznam so vsetkymi ulozenymi ID
 */
bool checkID(char tag_id[10], char uid[][10]);

#endif
