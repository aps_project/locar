#ifndef _LOCAR_DATABASE_H
#define _LOCAR_DATABASE_H

/**
 * Inicializuje SD kartu
 *
 * Vypise na seriovy port ci bola SD karta uspesne inicializovana alebo nie. 
 */
void sdSetup();


/**
 * Nacita pouzivatelov z textoveho suboru na SD karte.
 * 
 * @param driver smernik na pole do ktoreho sa nacitaju mena pouzivatelov
 * @param uid smernik na pole do ktoreho sa nacitaju UID kariet pouzivatelov
 */
void loadUsers(char driver[][10], char uid[][10]);

/**
 * Prida pouzivatela do textoveho suboru na SD karte.
 *
 * @param driver reprezentuje meno pouzivatela
 * @param uid reprezentuje UID karty pouzivatela
 */
void addUser(String driver, String uid);

/**
 * Vymaze meno pouzivatela a jeho UID karty z textoveho suboru na karte
 *
 * @param toDelete reprezentuje meno pouzivatela alebo uid karty pouzivatela
 *                 (funkcii je jedno ci to je uid alebo meno)
 * @param driver smernik na pole vsetkych uzivatelov - tato funkcia vycisti
 *               cele pole a nacita do nej nove data z textoveho suboru
 * @param uid smernik na pole vsetkych UID kariet pouzivatelov - tato funkcia vycisti
 *            cele pole a nacita do nej nove data z textoveho suboru
 */
void deleteUser(char toDelete[], char driver[][10], char uid[][10]);

#endif
